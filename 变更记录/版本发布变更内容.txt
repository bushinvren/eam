Foxnic-EAM 2.6.0
1、新增数据权限支持按组织查询
2、新增流程支持上一级直属领导角色审批
3、完善系统主题，主题不一致bug
4、修复启动CPU使用率过高问题
5、新增移动端人口定义
6、优化设备巡检逻辑
7、设备管理增加位置经纬度信息
8、修复我的资产查询数据记录
9、增加企业门户，公告通知管理模块
10、增强首页动画效果，支持背景图片切换
11、增加流程门户功能，支持在流程门户发起流程审批
12、修复数据字典按照模块自动查询无数据问题
13、优化自动安装脚本，以及维护脚本
14、修复数据中心机柜试图显示问题




Foxnic-EAM 2.5.0
1、调整参数位置
2、优化登录界面逻辑
3、提高EAM操作逻辑
4、后台系统新数据库备份菜单
5、新增新用户默认权限赋予功能
6、新增系统节点负载面板
7、资产管理新增直接显示二维码


Foxnic-EAM 2.4.0
1、新增资产复制功能
2、升级Foxnic-web
3、修复资产批量入库无法选择资产问题
4、增加支持手动输入资产编号功能
5、已离职资产待处理报表、资产操作汇总报表
6、支持在资产编号自动生成时，支持所属公司、资产分类编码字段
7、资产员工全员盘点，并且支持手机端全员盘点
8、维保增加维保方式建议和维保方式配置
9、增加批量维保更新
10、优化资产折旧，显示资产折旧历史记录
11、优化资产导入界面
12、支持专用标签打印机进行标签打印



Foxnic-EAM 2.3.0版本
1、完善界面
2、修复bug
3、新增密文箱功能

Foxnic-EAM 2.2.0版本
1、功能优化

Foxnic-EAM 2.1.0版本
1、资产位置修改为树形结构
2、优化前端UI
3、修改相关Bug
4、用户新增后自动配置权限


Foxnic-EAM 1.0.12版本
1、完善保养
2、完善报修
3、完善巡检
4、修复若干Bug


Foxnic-EAM 1.0.11版本
1、新增根据资产单据入库
2、新增基于在线Web Excel方式入库
3、新增资产标签模版在线设置
4、新增界面资产编号自定义设置
5、修改导入的Bug